node ('build') {

    env.VERSION="0.0.1"
    //this version depends on branch where app was built
    env.APP_VERSION=getAppVersion()

    stage('Checkout') {
        checkout scm
    }

    withCredentials([string(credentialsId: 'anodot-jwt-sign-key', variable: 'ANODOT_SIGN_KEY')]) {
    stage('Build'){
        sh 'docker build --build-arg ANODOT_SING_KEY=${ANODOT_SIGN_KEY} -t anodot-operator:${APP_VERSION} --label "version=${APP_VERSION}" --label "branch_name=${BRANCH_NAME}" --label "build_number=${BUILD_NUMBER}" .'
    }
    }

    stage('Push'){
        sh "export AWS_DEFAULT_REGION=us-east-1; aws ecr get-login | sed -e 's#-e none# #g' | bash"
        sh "docker tag anodot-operator:${APP_VERSION} 340481513670.dkr.ecr.us-east-1.amazonaws.com/anodot-operator:${APP_VERSION}"
        sh "docker push 340481513670.dkr.ecr.us-east-1.amazonaws.com/anodot-operator:${APP_VERSION}"
    }

    stage("Publish helm"){
        buildHelm()
    }
}


void buildHelm(){
    dir("${WORKSPACE}/helm/anodot-operator"){
    withEnv(["AWS_REGION=us-east-1"]) {
        //https://github.com/helm/helm/issues/1732
        sh "helm init --client-only"
        sh "helm plugin install https://github.com/hypnoglow/helm-s3.git || echo 'failed to install plugin...ignoring error' "
        sh "helm repo add anodot s3://anodot-helm-charts/charts"

        def valuesYaml = readYaml file: 'values.yaml'
        valuesYaml.image.tag = "${APP_VERSION}"

        echo "The Values $valuesYaml"
        sh "mv values.yaml values.yaml.org"
        writeYaml file: 'values.yaml', data: valuesYaml

        upgradeChartVersion()
        if (fileExists('requirements.yaml')){
            sh "helm dep update ."
        }

        def chartYaml = readYaml file: 'Chart.yaml'
        chartVersion = chartYaml.version

        sh "helm package ."
        sh "helm s3 push --force ./anodot-operator-${chartVersion}.tgz anodot"
        }
    }
}

    void upgradeChartVersion() {
        def chartYaml = readYaml file: 'Chart.yaml'
        chartYaml.appVersion = "${APP_VERSION}"
        chartYaml.version = "${APP_VERSION}"

        sh "mv Chart.yaml Chart.yaml.org"
        writeYaml file: 'Chart.yaml', data: chartYaml
}

String getAppVersion(){
    if ("master" == "${BRANCH_NAME}"){
        return "${VERSION}"
    }else {
        return "${VERSION}-${BRANCH_NAME}"
    }
}
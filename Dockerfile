FROM golang:1.12.5-stretch as builder
ARG ANODOT_SING_KEY

COPY . /anodot-operator
WORKDIR /anodot-operator
# sync to resolve https://github.com/moby/moby/issues/9547
RUN chmod +x *.sh && sync && ./build.sh ${ANODOT_SING_KEY}

FROM alpine:3.6
COPY --from=builder /anodot-operator/anodot-operator /usr/local/bin/anodot-operator

RUN adduser -D anodot-operator && chmod +x /usr/local/bin/anodot-operator
USER anodot-operator

ENTRYPOINT ["/usr/local/bin/anodot-operator"]
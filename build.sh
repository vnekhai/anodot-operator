#!/usr/bin/env bash

export GO111MODULE=on
export CGO_ENABLED=0
export GOOS=linux
export GOFLAGS=-mod=vendor

go env


sourceFiles=$(go list ./... | grep -v vendor/)

set -x
files=$(gofmt -l . | grep -v vendor/)
if [[ ! -z "${files}" ]]; then
    echo "Wrong formatted files: ${files}"
    exit 1
fi

go vet ${sourceFiles}
go test  -ldflags "-X bitbucket.org/anodotengineering/anodot-jwt/licence.AnodotLicenceSingKey=${1}" -v ${sourceFiles}
go build -ldflags "-X bitbucket.org/anodotengineering/anodot-jwt/licence.AnodotLicenceSingKey=${1}" -o anodot-operator
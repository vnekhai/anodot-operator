package licence

import "testing"
import anjwt "bitbucket.org/anodotengineering/anodot-jwt/licence"

//key is set during build time. Just to make sure that everything is set correctly. NewAnodotJWTIssuer() will panic if key not set
func TestPrivateKeySet(t *testing.T) {
	anjwt.NewAnodotJWTIssuer()
}

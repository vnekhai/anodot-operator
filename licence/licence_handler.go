package licence

import (
	anjwt "bitbucket.org/anodotengineering/anodot-jwt/licence"
	"context"
	"fmt"
	"github.com/operator-framework/operator-sdk/pkg/sdk"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"log"
	"os"
	"strconv"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/util/retry"
	"strings"
	"sync/atomic"
	"time"
)

const scaledDownLabel = "anodot.scaled.from"

const defaultVersobe = false
const defaultNamespace = "default"
const defaultLicencePollTime = time.Duration(720 * time.Minute)
const defaultFailureThreshold = 3
const defaultDeploymentNameToShutDown = "webapp"

var (
	validationFailed = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "anodot_licence_validation_fails",
		Help: "Number of failures occurred during licence validation",
	})

	licenceValidTill = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "anodot_licence_valid_until_days",
		Help: "Number of days till licence expiration",
	})

	licenceFailureTrashhold = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "anodot_licence_failure_threshold",
		Help: "Number of licence failures allowed, before shutting down cluster",
	})

	clusterShutdownCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "anodot_cluster_shutdown_count",
		Help: "Number of times Anodot cluster has been shut down",
	})
)

var usageInfo = `
------------------------------------------------------------------------------------------------------------------------------------
Cluster licence management information
------------------------------------------------------------------------------------------------------------------------------------

To create licence use next command:
	kubectl create secret generic %[1]v --from-file=<licence_file_path>

To update licence:
	kubectl create secret generic %[1]v --from-file=<licence_file_path> --dry-run -o yaml | kubectl apply -f -

NOTES:
	licence_file_path - should be a file with any extension, which will have licence string provided by Anodot company inside.
------------------------------------------------------------------------------------------------------------------------------------
`

func (lh *LiceneHandler) printInfo() {
	fmt.Printf(usageInfo, lh.SecretName)
}

//TODO vnekhai
// 1. create distributed lock if running more that 1 instance
type LiceneHandler struct {
	apiClient *kubernetes.Clientset
	jwtIssuer anjwt.JWTIssuer

	Config
	currentFailures uint32

	eventChannel chan *v1.Secret

	metricsRegistry *prometheus.Registry
}

type Config struct {
	Namespace  string
	SecretName string
	//Number of failures occurred during licence validation
	FailureThreshold uint32
	//how often to verify licence
	PollTime time.Duration

	licenceAffectedDeployment string

	verbose bool
}

func (c Config) NewLicenceHandler(apiclient *kubernetes.Clientset) (*LiceneHandler, error) {

	if len(strings.TrimSpace(c.Namespace)) == 0 {
		return nil, errors.New("namespace should not be empty")
	}

	if len(strings.TrimSpace(c.SecretName)) == 0 {
		return nil, errors.New("secret name should not be empty")
	}

	if apiclient == nil {
		return nil, errors.New("apiclient should not be nil")
	}

	registry := prometheus.NewRegistry()
	registry.MustRegister(validationFailed, licenceValidTill, licenceFailureTrashhold)

	return &LiceneHandler{metricsRegistry: registry, eventChannel: make(chan *v1.Secret), Config: c, apiClient: apiclient, jwtIssuer: anjwt.NewAnodotJWTIssuer()}, nil
}

func (lh *LiceneHandler) Run() {
	lh.printInfo()

	ticker := time.NewTicker(lh.PollTime)
	validateSecret := func() {
		secret, _ := lh.apiClient.CoreV1().Secrets(lh.Namespace).Get(lh.SecretName, meta_v1.GetOptions{})
		lh.handleLicence(secret)
	}

	validateSecret()
	go func() {
		for {
			select {
			case _ = <-ticker.C:
				validateSecret()
			case secret := <-lh.eventChannel:
				lh.handleLicence(secret)
			}
		}
	}()
}

func (lh *LiceneHandler) Handle(ctx context.Context, event sdk.Event) error {
	deleted := event.Deleted
	switch secret := event.Object.(type) {
	case *v1.Secret:
		if lh.SecretName == secret.Name {
			lh.logDebug(fmt.Sprintf("processing kubernetes secret event. deleted=%t", deleted))
			if deleted {
				//secret is no longer present in system, so there is not data available.
				secret.Data = map[string][]byte{}
			}
			lh.eventChannel <- secret
		}
		return nil
	}
	return nil
}

func NewConfigFromEnv() (Config, error) {
	namespace := os.Getenv("WATCH_NAMESPACE")
	if len(strings.TrimSpace(namespace)) == 0 {
		namespace = defaultNamespace
	}

	secretName := os.Getenv("ANODOT_LICENCE_SECRET_NAME")
	if len(strings.TrimSpace(namespace)) == 0 {
		return Config{}, errors.New("'ANODOT_LICENCE_SECRET_NAME' is not set")
	}

	pollTime := defaultLicencePollTime
	pollTimeStr := os.Getenv("LICENCE_POLL_TIME_MIN")
	if len(strings.TrimSpace(pollTimeStr)) != 0 {
		if i, err := strconv.Atoi(pollTimeStr); err != nil {
			log.Println(fmt.Sprintf("Failed to parse env varaible `LICENCE_POLL_TIME_MIN` with value %q as integer. Using default: %q", pollTimeStr, pollTime))
		} else {
			pollTime = time.Duration(time.Duration(i) * time.Minute)
		}
	}

	failureThreshold := defaultFailureThreshold
	failureThresholdStr := os.Getenv("LICENCE_FAILURE_THRESHOLD")
	if len(strings.TrimSpace(failureThresholdStr)) != 0 {
		if i, err := strconv.Atoi(failureThresholdStr); err != nil {
			log.Println(fmt.Sprintf("Failed to parse env varaible `LICENCE_FAILURE_THRESHOLD` with value %q as integer. Using default: %q", failureThresholdStr, failureThreshold))
		} else {
			failureThreshold = i
		}
	}

	verbose, e := strconv.ParseBool(os.Getenv("VERBOSE"))
	if e != nil {
		log.Println(fmt.Sprintf("Failed to parse `VERBOSE` with value %q.", os.Getenv("VERBOSE")))
		verbose = defaultVersobe
	}

	var licenceAffectedDeployment = defaultDeploymentNameToShutDown
	s := strings.TrimSpace(os.Getenv("LICENCE_AFFECTED_DEPLOYMENT"))
	if len(s) != 0 {
		licenceAffectedDeployment = s
	}

	config := Config{Namespace: namespace, SecretName: secretName, licenceAffectedDeployment: licenceAffectedDeployment, FailureThreshold: 3, PollTime: pollTime, verbose: verbose}
	licenceFailureTrashhold.Set(float64(config.FailureThreshold))
	return config, nil
}

func (lh *LiceneHandler) isScaledDown() (bool, error) {
	deploymentList, e := lh.apiClient.AppsV1().Deployments(lh.Namespace).List(meta_v1.ListOptions{LabelSelector: scaledDownLabel})
	if e != nil {
		return false, e
	}

	lh.logDebug(fmt.Sprintf("Found %d deployment(s) with label %q", len(deploymentList.Items), scaledDownLabel))
	for _, v := range deploymentList.Items {
		currentReplicas := *v.Spec.Replicas
		if currentReplicas != 0 {
			lh.logDebug(fmt.Sprintf("%q deployment supposed to be scaled down already. Setting isScaledDown=false", v.Name))
			return false, nil
		}
	}

	scaledDown := len(deploymentList.Items) != 0
	return scaledDown, nil
}

func (lh *LiceneHandler) restoreCluster() error {
	log.Println("Restoring anodot cluster...")
	deploymentList, e := lh.apiClient.AppsV1().Deployments(lh.Namespace).List(meta_v1.ListOptions{LabelSelector: scaledDownLabel})
	if e != nil {
		return e
	}

	lh.logDebug(fmt.Sprintf("Found %d deployment(s) matching label %q", len(deploymentList.Items), scaledDownLabel))
	for _, v := range deploymentList.Items {
		prevNumberOfInstances, _ := strconv.Atoi(v.Labels[scaledDownLabel])
		err := lh.scaleDeployment(lh.licenceAffectedDeployment, prevNumberOfInstances, func(d *appsv1.Deployment) {
			labels := v.Labels
			delete(labels, scaledDownLabel)
			d.Labels = labels
		})
		if err != nil {
			log.Println(fmt.Sprintf("Failed to restore deployment %q : %+v\n", v.Name, err))
		}
	}

	return e
}

func (lh *LiceneHandler) shutDownCluster() error {
	var shutdownError error

	shutdownError = lh.scaleDeployment(lh.licenceAffectedDeployment, 0, func(d *appsv1.Deployment) {
		if len(d.Labels) == 0 {
			d.Labels = make(map[string]string)
		}

		d.Labels[scaledDownLabel] = strconv.Itoa(int(*d.Spec.Replicas))
	})

	if shutdownError != nil {
		return shutdownError
	} else {
		clusterShutdownCounter.Inc()
		return nil
	}
}

func (lh *LiceneHandler) handleLicence(secret *v1.Secret) {
	var jwttoken = ""
	var anodotLicence anjwt.AnodotLicence
	if secret != nil && len(secret.Data) == 1 {
		for k, v := range secret.Data {
			lh.logDebug(fmt.Sprintf("Found licence key %q", k))
			jwttoken = string(v)
		}

		anodotLicence = lh.jwtIssuer.NewAnodotLicence(jwttoken)
		lh.logDebug(fmt.Sprintf("Licence details: %+v", anodotLicence))

	} else {
		anodotLicence.ValidationError = errors.New(fmt.Sprintf("unable to find anodot licence as secret with name %q", lh.SecretName))
	}

	if anodotLicence.ExpiresAt == nil {
		licenceValidTill.Set(0)
	} else {
		licenceValidTill.Set(anodotLicence.ExpiresAt.Sub(time.Now().UTC()).Hours() / 24)
	}

	if !anodotLicence.Valid && lh.currentFailures < uint32(lh.FailureThreshold) {
		log.Println(fmt.Sprintf("failed attempt of validtating licence (%d/%d). Error: %q", lh.incFailedCount(), lh.FailureThreshold, anodotLicence.ValidationError))
		return
	}

	isScaledDown, err := lh.isScaledDown()
	if err != nil {
		log.Println("unable to determine if anodot cluster being scaled down. Assuming isScaledDown=false.", err)
		isScaledDown = false
	}

	if !anodotLicence.Valid && lh.currentFailures >= uint32(lh.FailureThreshold) {
		if !isScaledDown {
			log.Println("Shutting down cluster. Licence is not valid. Please, contact administrator to provide new licence")
			err = lh.shutDownCluster()
			if err != nil {
				log.Println(fmt.Sprintf("Failed to shutdown cluster. Will try next time: %+v\n", err))
			}
			return
		}
		log.Println("Anodot cluster if already down. Please, contact administrator to provide new licence")
	}

	if anodotLicence.Valid {
		lh.resetFailedCount()
		if isScaledDown {
			err := lh.restoreCluster()
			if err != nil {
				log.Println(fmt.Sprintf("Failed to restore anodot cluster replicas number: %+v\n", err))
			}
		}
		return
	}
}

func (lh *LiceneHandler) resetFailedCount() {
	atomic.StoreUint32(&lh.currentFailures, 0)
	validationFailed.Set(0)
}

func (lh *LiceneHandler) incFailedCount() uint32 {
	atomic.AddUint32(&lh.currentFailures, 1)
	validationFailed.Set(float64(lh.currentFailures))
	return lh.currentFailures
}

func (lh *LiceneHandler) scaleDeployment(deploymentName string, replicaNumber int, modify func(d *appsv1.Deployment)) error {
	retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {

		deployment, err := lh.apiClient.AppsV1().Deployments(lh.Namespace).Get(lh.licenceAffectedDeployment, meta_v1.GetOptions{IncludeUninitialized: true})
		if err != nil {
			return err
		}

		if deployment == nil {
			return errors.New(fmt.Sprintf("unable to find deployment with name=%q", lh.licenceAffectedDeployment))
		}

		modify(deployment)

		lh.logDebug(fmt.Sprintf("found deployment %q with current number of replicas %d. Desired number of replicas=%d", deployment.Name, *deployment.Spec.Replicas, replicaNumber))
		desiredReplicas := int32(replicaNumber)
		if *deployment.Spec.Replicas == desiredReplicas {
			lh.logDebug(fmt.Sprintf("%q has already %d instances. Nothing to do...", deployment.Name, replicaNumber))
			return nil
		}

		deployment.Spec.Replicas = &desiredReplicas
		_, err = lh.apiClient.AppsV1().Deployments(lh.Namespace).Update(deployment)
		if err == nil {
			lh.logDebug(fmt.Sprintf("%q was scaled to %d instances", deployment.Name, *deployment.Spec.Replicas))
			return nil
		} else {
			return err
		}
	})

	return retryErr
}

func (lh *LiceneHandler) logDebug(v ...interface{}) {
	log.SetPrefix("[DEBUG] ")
	defer func() {
		log.SetPrefix("")
	}()

	if lh.verbose {
		log.Println(v...)
	}
}

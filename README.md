# Anodot operator

* [Description](#description)
* [Configuration](#configuration)
* [Installation](#installation)
  * [Required env variables](#required-env-variables)
  * [Supported env variables](#supported-env-variables)
* [Features](#features)
  * [Licence validation](#licence-validation)

# Description
Anodot operator used to manage onpem deployments. Currently supported features:
- Licence validation

# Configuration

## Required env variables
| Env variable                  | Description| 
| ------------------------------| -----------|
| ANODOT_LICENCE_SECRET_NAME    | Kubernetes secret name which contains licence key |

## Supported env variables
| Env variable                  | Description| Default value |
| ------------------------------| -----------|-------------|
| WATCH_NAMESPACE               | Kubernetes namespace to watch for objects | default |
| LICENCE_POLL_TIME_MIN         | How frequent licence validation will be preformed. Should be set in minutes    | 720|
| VERBOSE                       | Include debug information to STDOUT | false |
| LICENCE_FAILURE_THRESHOLD     | Number of licence failures allowed, before shutting down cluster | 3 |
| LICENCE_AFFECTED_DEPLOYMENT   | Kubernetes deployment name, which will be shut down when licence will be consider as invalid| `webapp` |

# Installation

TODO: steps to install as helm chart or single kubernetes deployment

# Features
Anodot operator provides prometheus metrics accessible by address `localhost:8080/metrics`

## Licence validation
Each onprem customer is provided with its own licence key which has jwt token inside. Each licence key has its own expiration date. 
When licence key has expired, customer should renew licence by contacting Anodot

If licence is expired/invalid/removed, cluster will be shut down, until licence will be fixed (renewed, re-created, etc)


### Uploading licence to cluster
**To upload licence to cluster use next command:**

```bash
    kubectl create secret generic anodot-licence --from-file=~/my_licence.jwt
```

In this case `ANODOT_LICENCE_SECRET_NAME` should be set to `anodot-licence`

Licence file should look like:
```bash
    cat ~/my_licence.jwt
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJmb3ItdGVzdCIsImV4cCI6MTU2MDg3MjQxOCwianRpIjoiMDIxM2I4N2MtMGUzNi00YmE3LWJjMjEtMjBlYzA1NTBkYTIxIiwiaXNzIjoiQW5vZG90In0.9EsSt_hM_k1XDZj9p44GbRTblFUqseQiHBXAJSEmdlo
```

JWT token could be at https://jwt.io/
 
**To update licence in cluster use next command:**
```bash
    kubectl create secret generic anodot-licence --from-file=~/new_licence.jwt --dry-run -o yaml | kubectl apply -f -
```

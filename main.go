package main

import (
	"anodot-operator/licence"
	"context"
	"fmt"
	"github.com/operator-framework/operator-sdk/pkg/sdk"
	sdkVersion "github.com/operator-framework/operator-sdk/version"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/util/wait"
	discovery "k8s.io/apimachinery/pkg/version"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"log"
	"net/http"
	"runtime"
	"time"
)

const (
	// High enough QPS to fit all expected use cases. QPS=0 is not set here, because
	// client code is overriding it.
	defaultQPS = 1e6
	// High enough Burst to fit all expected use cases. Burst=0 is not set here, because
	// client code is overriding it.
	defaultBurst = 1e6
)

func createApiserverClient(apiserverHost, kubeConfig string) (*kubernetes.Clientset, error) {
	cfg, err := clientcmd.BuildConfigFromFlags(apiserverHost, kubeConfig)
	if err != nil {
		return nil, err
	}

	cfg.QPS = defaultQPS
	cfg.Burst = defaultBurst
	cfg.ContentType = "application/vnd.kubernetes.protobuf"

	log.Printf("Creating API client for %s", cfg.Host)

	client, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	var v *discovery.Info

	// The client may fail to connect to the API server in the first request.
	// https://github.com/kubernetes/ingress-nginx/issues/1968
	defaultRetry := wait.Backoff{
		Steps:    10,
		Duration: 1 * time.Second,
		Factor:   1.5,
		Jitter:   0.1,
	}

	var lastErr error
	retries := 0
	log.Println("Trying to discover Kubernetes version")
	err = wait.ExponentialBackoff(defaultRetry, func() (bool, error) {
		v, err = client.Discovery().ServerVersion()

		if err == nil {
			return true, nil
		}

		lastErr = err
		log.Printf("Unexpected error discovering Kubernetes version (attempt %v): %v", retries, err)
		retries++
		return false, nil
	})

	// err is returned in case of timeout in the exponential backoff (ErrWaitTimeout)
	if err != nil {
		return nil, lastErr
	}

	// this should not happen, warn the user
	if retries > 0 {
		logrus.Warningf("Initial connection to the Kubernetes API server was retried %d times.", retries)
	}

	log.Printf("Running in Kubernetes cluster version v%v.%v (%v) - git (%v) commit %v - platform %v",
		v.Major, v.Minor, v.GitVersion, v.GitTreeState, v.GitCommit, v.Platform)

	return client, nil
}

func printVersion() {
	log.Printf("Go Version: %s", runtime.Version())
	log.Printf("Go OS/Arch: %s/%s", runtime.GOOS, runtime.GOARCH)
	log.Printf("operator-sdk Version: %v", sdkVersion.Version)
}

func main() {
	printVersion()

	apiClient, err := createApiserverClient("", "")
	if err != nil {
		log.Fatalf("Failed to create kubernetes api client: %+v\n", err)
	}

	config, err := licence.NewConfigFromEnv()
	if err != nil {
		log.Fatalf("Failed to prepare application configuration: %+v\n", err)
	}

	liceneHandler, err := config.NewLicenceHandler(apiClient)
	if err != nil {
		log.Fatalf("Failed to create Anodot Licence Handler: %+v\n", err)
	}

	log.Printf("Starting operator with configuration: %+v", config)

	go func() {
		http.Handle("/metrics", promhttp.Handler())
		http.HandleFunc("/health", healthHandler)
		e := http.ListenAndServe(":8080", nil)
		if e != nil {
			log.Println("Failed to expose Prometheus metrics. Metrics will be unavailable")
		}
	}()

	liceneHandler.Run()
	sdk.Watch("v1", "Secret", config.Namespace, 0)
	sdk.Handle(liceneHandler)
	sdk.Run(context.TODO())
}

func healthHandler(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintln(writer, "ok")
}

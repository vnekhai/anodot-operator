package licence

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"strings"
	"time"
)

const (
	anodotIssuerName = "Anodot"
)

//set during build
var AnodotLicenceSingKey string

type JWTIssuer struct {
	name       string
	privateKey string
}

type AnodotLicence struct {
	CustomerName string
	ExpiresAt    *time.Time
	details
}

type details struct {
	Valid           bool
	ValidationError error
}

func NewAnodotJWTIssuer() JWTIssuer {
	issuer := JWTIssuer{name: anodotIssuerName, privateKey: AnodotLicenceSingKey}
	if len(strings.TrimSpace(issuer.privateKey)) == 0 {
		panic("Anodot licence sign key was not set")
	}
	return issuer
}

func (jwtIssuer JWTIssuer) CreateClaim(audience string, expiresAt time.Time) (string, error) {
	if len(strings.TrimSpace(audience)) == 0 {
		return "", errors.New("audience should not be blank")
	}

	if expiresAt.UTC().Before(time.Now().UTC()) {
		return "", errors.New(fmt.Sprintf("expiresAt should not be at past. Provided value is %q", expiresAt))
	}

	// Create the Claims
	claims := &jwt.StandardClaims{
		Id:        uuid.New().String(),
		ExpiresAt: expiresAt.UTC().Unix(),
		Issuer:    jwtIssuer.name,
		Audience:  audience,
	}

	// Create the token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(jwtIssuer.privateKey))
	return tokenString, err
}

func (jwtIssuer JWTIssuer) NewAnodotLicence(jwtToken string) AnodotLicence {
	isValid, t, validationError := jwtIssuer.IsValidClaim(jwtToken)

	anodotLicence := AnodotLicence{}
	anodotLicence.Valid = isValid
	anodotLicence.ValidationError = validationError

	if t == nil || t.Claims == nil {
		anodotLicence.ValidationError = errors.Wrap(validationError, "jwt token has wrong format")
		return anodotLicence
	}

	claims, ok := t.Claims.(jwt.MapClaims)
	if !ok {
		anodotLicence.ValidationError = errors.Wrap(validationError, "unable to parse jwt claims")
		return anodotLicence
	}

	for _, requiredField := range []string{"exp", "aud"} {
		if _, ok := claims[requiredField]; !ok {
			anodotLicence.ValidationError = errors.Wrap(validationError, fmt.Sprintf("jwt token missing required field %q", requiredField))
			return anodotLicence
		}
	}

	for k, v := range claims {
		switch k {
		case "exp":
			tm := time.Unix(int64(v.(float64)), 0)
			v := tm.UTC()
			anodotLicence.ExpiresAt = &v
		case "aud":
			anodotLicence.CustomerName = v.(string)
		}
	}
	return anodotLicence
}

func (jwtIssuer JWTIssuer) IsValidClaim(claim string) (isValid bool, t *jwt.Token, err error) {
	token, err := jwt.Parse(claim, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwtIssuer.privateKey), nil
	})

	if err == nil && token.Valid {
		return true, token, nil
	} else {
		return false, token, err
	}
}

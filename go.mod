module anodot-operator

go 1.12

require (
	bitbucket.org/anodotengineering/anodot-jwt v0.0.0-20190703083510-a0718e04cd50
	github.com/PuerkitoBio/purell v1.1.0 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/emicklei/go-restful v2.8.0+incompatible // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-openapi/jsonpointer v0.0.0-20180322222829-3a0015ad55fa // indirect
	github.com/go-openapi/jsonreference v0.0.0-20180322222742-3fb327e6747d // indirect
	github.com/go-openapi/spec v0.0.0-20180710175419-bce47c9386f9 // indirect
	github.com/go-openapi/swag v0.0.0-20180703152219-2b0bd4f193d0 // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/golang/groupcache v0.0.0-20190129154638-5b532d6fd5ef // indirect
	github.com/google/btree v0.0.0-20180124185431-e89373fe6b4a // indirect
	github.com/google/gofuzz v0.0.0-20170612174753-24818f796faf // indirect
	github.com/googleapis/gnostic v0.2.0 // indirect
	github.com/gregjones/httpcache v0.0.0-20180305231024-9cad4c3443a7 // indirect
	github.com/hashicorp/golang-lru v0.0.0-20180201235237-0fb14efe8c47 // indirect
	github.com/howeyc/gopass v0.0.0-20170109162249-bf9dde6d0d2c // indirect
	github.com/imdario/mergo v0.3.5 // indirect
	github.com/juju/ratelimit v1.0.1 // indirect
	github.com/mailru/easyjson v0.0.0-20180717111219-efc7eb8984d6 // indirect
	github.com/operator-framework/operator-sdk v0.0.0-20180719224938-57feeea77715
	github.com/peterbourgon/diskv v2.0.1+incompatible // indirect
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v1.0.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/pflag v1.0.1 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	k8s.io/api v0.0.0-20180127130940-acf347b865f2
	k8s.io/apimachinery v0.0.0-20180126010752-19e3f5aa3adc
	k8s.io/client-go v0.0.0-20180103015815-9389c055a838
	k8s.io/kube-openapi v0.0.0-20180719232738-d8ea2fe547a4 // indirect
)
